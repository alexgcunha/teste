import React from 'react';
import {View} from 'react-native';

import styles from './style';

import Posts from './Posts';


export default class index extends Component {
    state = {
        posts: [{
            id: 1,
            image:  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqxRlAXxSNS9SGIe_RWTAzcEVREdEcdjHnnLTIO7FVg8Spb25VpQ',
            nome: 'Heyley',
            medico: 'fulano',
            especialidade:'pediatra',
            data: '04/09/2018',
            fixa: '10'

            
        }
     ],
    };
    render () {
        return (
            <View style={styles.container}>
                { this.state.posts.map(post => 
                <Post key={post.id} posts={post}/>
                )}
            
            </View>
        );
    }
}