import React from 'react';

import { View, Text, Image } from 'react-native';

import styles from './style';


const Posts = ({ Post: { image, nome, medico, especialidade, data, fixa} }) =>(
<View style={styles.container}>
    <Image source={{ uri: image }} style={styles.image} />
    <Text>style={styles.nome}{nome}</Text>
    <Text>style={styles.info}{medico}</Text>
    <Text>style={styles.info}{especialidade}</Text>
    <Text>style={styles.info}{data}</Text>
    <Text>style={styles.info}{fixa}</Text>
</View>
);

export default Post;
