import React from 'react';
import { View, Text, Image} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './style';
const HeaderHome = () =>
(
 <View style={styles.container}>
      <Image 
        style={styles.avatar}
        source={{uri : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqxRlAXxSNS9SGIe_RWTAzcEVREdEcdjHnnLTIO7FVg8Spb25VpQ'}}
      />

        <Text style={styles.textHome}>Sus Colaborativo</Text>
        <Icon name="ios-reorder" size={38} style={styles.icon}/>

</View>
);

export default HeaderHome;