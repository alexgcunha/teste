import React, { Component } from 'react';
import { StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        height: 44,
        paddingTop: 0,
        paddingHorizontal: 15,
        borderBottomWidth: 2,
        borderColor:'#111',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    main:{
        width:30,
        height:30,
        borderRadius: 15,
        backgroundColor:'#111',
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon:{
        color: '#fff'
    },
});

export default styles;
