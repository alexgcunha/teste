import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './style';
const IconBar = () =>(

    <View style={styles.container}>
    <View style={styles.main}>
        <Icon name="plus" size={ 24}style={styles.icon} />
    </View>
    </View>
    );
  export default IconBar;

