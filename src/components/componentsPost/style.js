import React, { Component } from 'react';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    repo:{
        padding: 20,
        backgroundColor: '#FFF',
        height:120,
        marginBottom: 5,
        borderRadius: 5,
        flexDirection: 'row',
        
        
        
    },
    avatar:{
            width:34,
            height:34,
            borderRadius:34,
            marginRight: 15,
            marginTop:5,
    },
    repoInfo:{
        marginLeft: 5,
        
    },
    repoNome:{
        fontWeight: 'bold',
        color:'#333'
    },
    icon:{
        
        marginLeft: 90,
    },
    info:{
        marginLeft: 20,
        
    }





});
 export default styles;