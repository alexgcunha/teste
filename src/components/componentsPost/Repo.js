import React, { Component } from 'react';
import {View, Text, Image} from 'react-native';
import styles from './style';
import Icon from 'react-native-vector-icons/Ionicons';
export default class Repo extends Component {
render(){
return(
 <View style={styles.repo}>
        <Image 
            style={styles.avatar}
            source={{uri : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqxRlAXxSNS9SGIe_RWTAzcEVREdEcdjHnnLTIO7FVg8Spb25VpQ'}}
        />

       
        <View style={styles.repoInfo}>
        <Text style={styles.repoNome}>Heyley Willians</Text>
            <Text style={styles.info}>medico: Dr. House</Text>
            <Text style={styles.info}>especialidade: Cirugião</Text>
            <Text style={styles.info}>data: 04/09/2018</Text>
            <Text style={styles.info}>fichas: 04</Text>
        </View>
        <View>
             <Icon name="ios-more"  size={20} style={styles.icon}/>
       </View>
</View>
    );
}
}