/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */


import React, { Component } from 'react';
import {  View, ScrollView, Text, StyleSheet } from 'react-native';
import HeaderHome from './components/headerHome';
import Repo from './components/componentsPost/Repo';
import styles from './components/headerHome/style';
import IconBar from './components/bar/index';

export default class App extends Component{
  state ={
    repos:[
      {
        id: 1,
        image: '',
        nome: 'Heyley Willians',
        medico: 'dr. House',
        especialidade: 'Cirugião',
        data: '04/09/2018',
        fichas:'fichas 20'
    },
    {
      id: 2,
      image: '',
      nome: 'Heyley Willians',
      medico: 'dr. House',
      especialidade: 'Cirugião',
      data: '04/09/2018',
      fichas:'fichas 20'
    },
    {
      id: 3,
      image: '',
      nome: 'Heyley Willians',
      medico: 'dr. House',
      especialidade: 'Cirugião',
      data: '04/09/2018',
      fichas:'fichas 20'
    },
    {
      id: 4,
      image: '',
      nome: 'Heyley Willians',
      medico: 'dr. House',
      especialidade: 'Cirugião',
      data: '04/09/2018',
      fichas:'fichas 20'
    },
    {
      id: 2,
      image: '',
      nome: 'Heyley Willians',
      medico: 'dr. House',
      especialidade: 'Cirugião',
      data: '04/09/2018',
      fichas:'fichas 20'
    }
  ],
};


  render(){
    return(
      <View style={{ flex: 1, backgroundColor: '#a6a6a6' }}>
      <HeaderHome />
        <ScrollView contentContainerStyle={{padding:5 }}>
           
          {this.state.repos.map(repo =>
             <Repo key={repo.id} style={styles.repo}/>
          )}
        </ScrollView>
        <IconBar />
  </View>
    );
  }
}


